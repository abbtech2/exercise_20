package org.abbtech.practice.mapper;

import org.abbtech.practice.dto.PostDto;
import org.abbtech.practice.entity.Post;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PostMapper {

    PostDto mapPostToDto(Post post);


    Post mapDtoToPost(PostDto postDto);
}
