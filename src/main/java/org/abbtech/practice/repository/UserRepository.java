package org.abbtech.practice.repository;

import org.abbtech.practice.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users,Long> {

    Optional<Users> findUserInfoByUsernameIgnoreCase(String username);
}
