package org.abbtech.practice.enumerations;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum ResponseStatusEnum {

    SUCCESS("200"),
    CREATED("201"),
    BAD_REQUEST("400"),
    UNAUTHORIZED("401"),
    FORBIDDEN("403");
    private final String statusCode;
}
