package org.abbtech.practice.controller;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.PostDto;
import org.abbtech.practice.dto.UsersDto;
import org.abbtech.practice.entity.Post;
import org.abbtech.practice.service.PostService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/post")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;


    @PostMapping(path = "/save")
    public void savePost(@RequestBody PostDto postDto) {
        postService.createPost(postDto);
    }

    @GetMapping(path = "/{id}")
    public Post getPostById(@PathVariable Long id) {
        return postService.findPostById(id);
    }

    @DeleteMapping(path = "/delete/{id}")
    public void deletePostById(@PathVariable Long id) {
        postService.deletePostById(id);
    }
}
