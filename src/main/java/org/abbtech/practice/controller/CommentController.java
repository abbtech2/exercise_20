package org.abbtech.practice.controller;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.PostDto;
import org.abbtech.practice.entity.Comment;
import org.abbtech.practice.entity.Post;
import org.abbtech.practice.service.CommentService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comment")
@RequiredArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @PostMapping(path = "/save")
    public void saveComment(@RequestBody Comment comment) {
        commentService.createComment(comment);
    }

    @GetMapping(path = "/{id}")
    public Comment getPCommentById(@PathVariable Long id) {
        return commentService.findCommentById(id);
    }
}
