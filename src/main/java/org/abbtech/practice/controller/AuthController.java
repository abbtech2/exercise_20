package org.abbtech.practice.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.AuthRequestDTO;
import org.abbtech.practice.dto.JwtResponseDTO;
import org.abbtech.practice.dto.UsersDto;
import org.abbtech.practice.service.JWTService;
import org.abbtech.practice.service.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@Validated
public class AuthController {

//    private final JWTService jwtService;
//
//    private final UserService userService;
//
//    private final AuthenticationManager authenticationManager;
//
//
//    @PostMapping(value = "/register")
//    public void saveUser(@RequestBody @Valid UsersDto userRequest) {
//        userService.saveUser(userRequest);
//    }
//
//    @PostMapping("/login")
//    public JwtResponseDTO authenticateAndGetToken(@RequestBody AuthRequestDTO authRequest) {
//        Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
//        if (authentication.isAuthenticated()) {
//            return JwtResponseDTO.builder()
//                    .accessToken(jwtService.generateToken(authRequest.getUsername()))
//                    .build();
//        } else {
//            throw new UsernameNotFoundException("invalid user request..!!");
//        }
//    }
}
