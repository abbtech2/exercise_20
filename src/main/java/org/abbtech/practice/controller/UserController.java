package org.abbtech.practice.controller;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.UsersDto;
import org.abbtech.practice.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping(path = "/save")
    public void saveUser(@RequestBody UsersDto user) {
        userService.saveUser(user);
    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<UsersDto>> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }



    @GetMapping(path = "/{id}")
    public ResponseEntity<UsersDto> getUSerById(@PathVariable Long id) {
        return ResponseEntity.ok(userService.findUserById(id));
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<UsersDto> updateUser(@PathVariable Long id, @RequestBody UsersDto usersDto) {
        return ResponseEntity.ok(userService.updateUser(id,usersDto));
    }

    @DeleteMapping(path = "/delete/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUserById(id);
    }
}
