package org.abbtech.practice.dto;

import jakarta.annotation.ManagedBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.context.annotation.RequestScope;

@Data
@ManagedBean
@RequestScope
@AllArgsConstructor
@NoArgsConstructor
public class RequestContext {

    private String path;
}
