package org.abbtech.practice.service;

import org.abbtech.practice.dto.PostDto;
import org.abbtech.practice.entity.Comment;
import org.abbtech.practice.entity.Post;

import java.util.List;

public interface CommentService {

    void createComment(Comment comment);

    Comment updatePost(Long id,Comment comment);

    Comment findCommentById(Long id);

    List<Comment> getAllComments();

    void deleteCommentById(Long id);
}
