package org.abbtech.practice.service;

import org.abbtech.practice.dto.PostDto;
import org.abbtech.practice.dto.UsersDto;
import org.abbtech.practice.entity.Post;

import java.util.List;

public interface PostService {

    void createPost(PostDto postDto);

    PostDto updatePost(Long id,PostDto postDto);

    Post findPostById(Long id);

    List<PostDto> getAllPosts();

    void deletePostById(Long id);
}
