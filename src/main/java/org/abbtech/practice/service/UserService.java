package org.abbtech.practice.service;

import org.abbtech.practice.dto.UsersDto;

import java.util.List;

public interface UserService {

    void saveUser(UsersDto user);

    UsersDto updateUser(Long id,UsersDto user);

    UsersDto findUserById(Long id);

    List<UsersDto> getAllUsers();

    void deleteUserById(Long id);
}
