package org.abbtech.practice.service.impl;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.entity.Comment;
import org.abbtech.practice.repository.CommentRepository;
import org.abbtech.practice.service.CommentService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    @Override
    public void createComment(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public Comment updatePost(Long id, Comment comment) {
        return null;
    }

    @Override
    public Comment findCommentById(Long id) {
        Comment comment = commentRepository.findById(id).get();
        return comment;
    }

    @Override
    public List<Comment> getAllComments() {
        return List.of();
    }

    @Override
    public void deleteCommentById(Long id) {

    }
}
