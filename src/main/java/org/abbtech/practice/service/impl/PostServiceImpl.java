package org.abbtech.practice.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.abbtech.practice.dto.PostDto;
import org.abbtech.practice.entity.Post;
import org.abbtech.practice.exception.RecordNotFoundException;
import org.abbtech.practice.mapper.PostMapper;
import org.abbtech.practice.mapper.PostMapperImpl;
import org.abbtech.practice.repository.PostRepository;
import org.abbtech.practice.service.PostService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final PostMapper postMapper;

    @Override
    public void createPost(PostDto postDto) {
        Post post = postMapper.mapDtoToPost(postDto);
        Long id = post.getId();
        log.info("ID : {}",id);
        postRepository.save(post);
    }

    @Override
    public PostDto updatePost(Long id, PostDto postDto) {
        return null;
    }

    @Override
    public Post findPostById(Long id) {
        return postRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("Post not found for id: " + id));
    }


    @Override
    public List<PostDto> getAllPosts() {
        return List.of();
    }

    @Override
    public void deletePostById(Long id) {
        postRepository.deleteById(id);
    }
}
