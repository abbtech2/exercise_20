package org.abbtech.practice.service.impl;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.UsersDto;
import org.abbtech.practice.entity.Users;
import org.abbtech.practice.exception.RecordNotFoundException;
import org.abbtech.practice.repository.UserRepository;
import org.abbtech.practice.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public void saveUser(UsersDto usersDto) {
        Users user = mapUserDtoToUsers(usersDto);
        userRepository.save(user);
    }

    @Override
    public UsersDto updateUser(Long id, UsersDto usersDto) {
        Users users = userRepository.findById(id).get();
        users.setUsername(usersDto.getUsername());
        users.setEmail(usersDto.getEmail());
        users.setPassword(usersDto.getPassword());
        userRepository.save(users);
        return mapUsersToUserDto(users);
    }

    @Override
    public UsersDto findUserById(Long id) {
        Optional<Users> findUser = userRepository.findById(id);
        if (findUser.isPresent()) {
            return mapUsersToUserDto(findUser.get());
        } else {
            throw new RecordNotFoundException("User not found ! ");
        }
    }

    @Override
    public List<UsersDto> getAllUsers() {
        List<UsersDto> userDtos;
        List<Users> allUsers = userRepository.findAll();
        userDtos = allUsers.stream().map(this::mapUsersToUserDto).toList();
        return userDtos;
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    private UsersDto mapUsersToUserDto(Users users) {
        return UsersDto.builder()
                .username(users.getUsername())
                .email(users.getEmail())
                .password(users.getPassword())
                .build();
    }

    private Users mapUserDtoToUsers(UsersDto usersDto) {
        return Users.builder()
                .username(usersDto.getUsername())
                .email(usersDto.getEmail())
                .password(usersDto.getPassword())
                .build();
    }
}